
{if $accessErrorFlag eq true} 
    
    <div id="accessDeniedMsg" class="SystemAdminFormPanel"  >   
    
        <form id="accessDeniedMsgForm" name="accessDeniedMsgForm" method="post"  action="#" class="inline">
                <fieldset>
                    <legend title="" > {$page['Text']['error_page_legend']|escape:'html'} </legend>
                    <p>

                        <label class="formCommonError" >{$accessDeniedMsg|escape:'html'}</label><br><br>

                    </p>

                    <p>

                        <span class= "bottomButtons" >


                            <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;"  value="{$page['Buttons']['ok']|escape:'html'}" >

                        </span>

                    </p>


                </fieldset>   
        </form>            


    </div>    
    
{else}  
  <script type="text/javascript" src="{$_subdomain}/js/jquery.combobox.js"></script>  
   <link rel="stylesheet" href="{$_subdomain}/css/themes/pccs/style.css" type="text/css" media="screen" charset="utf-8" />
 <script type="text/javascript">   
     
  (function ($, undefined) {
    $.fn.getCursorPosition = function () {
        var el = $(this).get(0);
        var pos = 0;
        if ('selectionStart' in el) {
            pos = el.selectionStart;
        } else if ('selection' in document) {
            el.focus();
            var Sel = document.selection.createRange();
            var SelLength = document.selection.createRange().text.length;
            Sel.moveStart('character', -el.value.length);
            pos = Sel.text.length - SelLength;
        }
        return pos;
    }
})(jQuery);

     
 $(document).ready(function() {
    
    
    
    $( "#BrandID" ).combobox();
    
    $('.ui-combobox-input').css('width', '300px');
    
    $('.fieldLabel').css('width', '200px');
    
    
        $(document).on('click', '#PlaceHolderLink', 
            function() {
                
                var position = $("#SMSBodyText").getCursorPosition();
                var content = $('#SMSBodyText').val();
                var newContent = content.substr(0, position) +" "+ $('#PlaceHolders').val() +" "+ content.substr(position);
                $('#SMSBodyText').val(newContent);
                
                
            });
    
       
    
    
    
   }); 
   
  </script>  
  
  <style type="text/css" >
      label.fieldError { margin-left:230px !important; }
  </style>
    
    <div id="smsFormPanel" class="SystemAdminFormPanel" >
    
                <form id="smsForm" name="smsForm" method="post"  action="#" class="inline" >
       
                <fieldset>
                    <legend title="" >{$form_legend|escape:'html'}</legend>
                        
                    <p><label id="suggestText" ></label></p>
                            <p>
                            <label ></label>
                            <span class="topText" >{$page['Text']['top_info_text1']|escape:'html'} <span>*</span> {$page['Text']['top_info_text2']|escape:'html'}</span>

                            </p>
                       
                         <p>
                            <label class="fieldLabel" for="BrandID" >{$page['Labels']['brand']|escape:'html'}:<sup>*</sup></label>
                           
                             &nbsp;&nbsp; 
                             
                             <select name="BrandID" id="BrandID"  >

                                <option value="" {if $datarow.BrandID eq ''}selected="selected"{/if}></option>

                                {foreach $brandsList as $brand}

                                    <option value="{$brand.BrandID|escape:'html'}" {if $datarow.BrandID eq $brand.BrandID} selected="selected" {/if}>{$brand.Name|escape:'html'} ({$brand.BrandID|escape:'html'}) </option>

                                {/foreach}

                             </select>
                        
                         </p>   
                            
                            
                         <p>
                            <label class="fieldLabel" for="SMSName" >{$page['Labels']['name']|escape:'html'}:<sup>*</sup></label>
                           
                             &nbsp;&nbsp; <input  type="text" class="text"  name="SMSName" value="{$datarow.SMSName|escape:'html'}" id="SMSName" >
                        
                         </p>
                         
                         
                          <p>
                            <label class="fieldLabel" for="SMSBodyText" >{$page['Labels']['sms_body']|escape:'html'}:<sup>*</sup></label>
                           
                             &nbsp;&nbsp; <textarea class="text" style="width:250px;height:240px;"  name="SMSBodyText" id="SMSBodyText" >{$datarow.SMSBodyText|escape:'html'}</textarea>
                        
                             <select size="10" id="PlaceHolders" name="PlaceHolders" style="width:170px;height:240px;" >
                                 
                                 <option value="[Manufacturer]" >[Manufacturer]</option>
                                 <option value="[Model]" >[Model]</option>
                                 <option value="[Branch Name]" >[Branch Name]</option>
                                 <option value="[Customer Surname]" >[Customer Surname]</option>
                                 <option value="[Customer Title]" >[Customer Title]</option>
                                 <option value="[Customer Forename]" >[Customer Forename]</option>
                                 <option value="[Booked By Full Name]" >[Booked By Full Name]</option>
                                 
                             </select>
                             <br>
                             <a href="#" style="float:right;text-decoration: underline;margin-right:25px;" id="PlaceHolderLink" >{$page['Text']['add_highlighted_place_holder']|escape:'html'}</a>
                          </p>
                         
                          
                         
                          {if $datarow.SMSID neq '' && $datarow.SMSID neq '0'}
                          
                              
                              <p>
                                <label class="fieldLabel" for="ModifiedDate" >{$page['Labels']['date_created']|escape:'html'}:<sup>*</sup></label>

                                 &nbsp;&nbsp; 
                                 
                                    <input  type="text" readonly="readonly" class="text" style="width:240px;border:0px;box-shadow: none;"  name="ModifiedDate" value="{$datarow.ModifiedDate|date_format:"%d/%m/%Y %H:%M:%S"} " id="ModifiedDate" >
                                  
                              </p>
                          
                              <p>
                                <label class="fieldLabel" for="ModifiedUserID" >{$page['Labels']['created_by']|escape:'html'}:<sup>*</sup></label>

                                 &nbsp;&nbsp; 
                                 
                                    <input  type="text" readonly="readonly" class="text" style="width:240px;border:0px;box-shadow: none;"  name="SMSUserName" value="{$datarow.SMSUserName|escape:'html'}" id="SMSUserName" >
                                  
                              </p>
                          
                          {/if}
                          
                          
                          <p>
                            <label class="fieldLabel" for="Status" >{$page['Labels']['status']|escape:'html'}:<sup>*</sup></label>
                           
                             &nbsp;&nbsp; 
                        
                                {foreach $statuses as $status}

                                    <input  type="radio" name="Status"  value="{$status.Code}" {if $datarow.Status eq $status.Code} checked="checked" {/if}  /> <span class="text" >{$status.Name|escape:'html'}</span> 

                                {/foreach}    


                          </p>
                         
                         

                            <p>
                    
                                <span class= "bottomButtons" >

                                    <input type="hidden" name="SMSID"  value="{$datarow.SMSID|escape:'html'}" >
                                   
                                    {if $datarow.SMSID neq '' && $datarow.SMSID neq '0'}
                                        
                                        <input type="submit" name="update_save_btn" class="textSubmitButton centerBtn" id="update_save_btn"  value="{$page['Buttons']['save']|escape:'html'}" >
                                    
                                    {else}
                                        
                                        <input type="submit" name="insert_save_btn" class="textSubmitButton centerBtn" id="insert_save_btn"  value="{$page['Buttons']['save']|escape:'html'}" >
                                        
                                    {/if}
                                   
                                        <br>
                                        <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;"  value="{$page['Buttons']['cancel']|escape:'html'}" >



                                </span>

                            </p>

                           
                          
                         
                         


                </fieldset>    
                        
                </form>        
                        
       
</div>
                 
{/if}  
                                    
                                    
{* This block of code is for to display message after data updation *} 

                                    
<div id="dataUpdatedMsg" class="SystemAdminFormPanel" style="display: none;" >   
    
    <form id="dataUpdatedMsgForm" name="dataUpdatedMsgForm" method="post"  action="#" class="inline">
            <fieldset>
                <legend title="" > {$form_legend|escape:'html'} </legend>
                <p>
                 
                    <b>{$page['Text']['data_updated_msg']|escape:'html'}</b><br><br>
                 
                </p>
                
                <p>

                    <span class= "bottomButtons" >


                        <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;"  value="{$page['Buttons']['ok']|escape:'html'}" >

                    </span>

                </p>
                          
                
            </fieldset>   
    </form>            
    
    
</div>    

    
 
  {* This block of code is for to display message after data insertion *}                      
                        
<div id="dataInsertedMsg" class="SystemAdminFormPanel" style="display: none;" >   
    
    <form id="dataInsertedMsgForm" name="dataInsertedMsgForm" method="post"  action="#" class="inline">
            <fieldset>
                <legend title="" > {$form_legend|escape:'html'} </legend>
                <p>
                 
                    <b>{$page['Text']['data_inserted_msg']|escape:'html'}</b><br><br>
                 
                </p>
                
                <p>

                    <span class= "bottomButtons" >


                        <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;"  value="{$page['Buttons']['ok']|escape:'html'}" >

                    </span>

                </p>
                          
                
            </fieldset>   
    </form>            
    
    
</div>                            
                        
