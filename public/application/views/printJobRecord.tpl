<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>{$printJob.PageTitle|escape:'html'}</title>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script>
<style>
.wrap{
	position:relative;float:left;zoom:1;margin:0px;padding-top:20px;
}
.receipt{
	position:absolute;
	top:{$brandLogoHeight}px;
	right:3px;
	color: #039CDF;
	font-family:Arial, Helvetica, sans-serif;
	line-height:18px;
	font-size: 16px;
	font-weight: bold;
	margin-top: 0;
	margin-bottom: 3px;
	text-align: left;
	text-transform:uppercase;
}
.logo{
	float:left;position:absolute;top:0px;left:0px;
}
html body div.wrap table tbody tr td.odd {
	border-top:5px solid #336699;
	padding-top:10px;
}
</style>
</head>

<body style="width: 100% !important; background-color: #ffffff; margin: 0; padding: 0px 0px 0px 40px;">
<div class="wrap">
	
    <img  style="outline: none; text-decoration: none; display: block;margin:3px 0 5px;" alt="{$printJob.Brand|escape:'html'}" title="{$printJob.Brand|escape:'html'}" src="{$brandLogo|escape:'html'}" width="{$brandLogoWidth}" height="{$brandLogoHeight}" />
    <table width="610" height="700" border="0" cellspacing="0" cellpadding="0"  >
  			<tr>
    		<td width="293" >&nbsp;</td>
   			<td width="22" >&nbsp;</td>
   			<td width="32" >&nbsp;</td>
    		<td width="272" align="right">
				<table align="right" style="padding:0px; margin:5px 0px 2px 0px;">
    				<tr><td style="margin-top: 0; font-family:Arial, Helvetica, sans-serif; font-size:11px;">
                                        <div class="receipt" >{$printJob.PageTitle|escape:'html'}</div>
                                        {if $printJob.ServiceCentreJobNo}
                                            
                                        SC Job Number: <span style="color:#666;font-weight:bold;margin-right:5px;">{$printJob.ServiceCentreJobNo|escape:'html'},
                                       
                                               
                                        {/if}    
                                            
                                            </span></td><td style="margin-top: 0; font-family:Arial, Helvetica, sans-serif; font-size:11px;">Date Booked: <span style="color:#666;font-weight:bold;margin-right:5px;">{$printJob.DateBooked|escape:'html'}</span></td>
    				</tr>
    					<tr style="margin-top: 0; font-family:Arial, Helvetica, sans-serif; font-size:11px;">
     
				    </tr>
    				</table>
  				</tr>
  			<tr>
    		<td class="odd"><h1 align="left" style="color: #039CDF; font-family:Arial, Helvetica, sans-serif;line-height:18px; font-size: 16px; font-weight: bold; margin-top: 0; margin-bottom: 0px; text-align: left; text-transform:uppercase;">Retailer</h1>
<table width="272" cellspacing="0" cellpadding="0" border="0" style="line-height:18px; font-family:Arial, Helvetica, sans-serif; font-size:11px;">
                      <tbody>
                       <tr height="20">
                    	<td width="209"><h4 style="margin:0px; font-weight:bold; text-transform:uppercase; color:#666666;">{$printJob.Branch|escape:'html'}</h4></td>
                       </tr>
    					<tr>
                        <td><h4 style="margin:0px; font-weight:normal;">{$printJob.BranchDetails1|default:'&nbsp;'}</h4></td>
                        </tr>
                        <tr>
                        <td><h4 style="margin:0px; font-weight:normal;">{$printJob.BranchDetails2|default:'&nbsp;'}</h4></td>
                        </tr>
                        <tr>
                        <td><h4 style="margin:0px; font-weight:normal;">{$printJob.BranchDetails3|default:'&nbsp;'}</h4></td>
                        </tr>
                        <tr>
                        <td><h4 style="margin:0px; font-weight:normal;">{$printJob.BranchDetails4|default:'&nbsp;'}</h4></td>
                        </tr>
                        <tr>
                        <td><h4 style="margin:0px; font-weight:normal;">{$printJob.BranchDetails5|default:'&nbsp;'}</h4></td>
                        </tr>
                        <tr>
                        <td><h4 style="margin:0px; font-weight:normal;">{$printJob.BranchDetails6|default:'&nbsp;'}</h4></td>
                        </tr>
                        <tr>
                        <td><h4 style="margin:0px; font-weight:normal;">{$printJob.BranchDetails7|default:'&nbsp;'}</h4></td>
                        </tr>
                        </tbody>
      				</table>
    		</td>
    		<td class="odd">&nbsp;</td>
    		<td class="odd">&nbsp;</td>
    		<td class="odd"><h1 align="left" style="color: #039CDF; font-family:Arial, Helvetica, sans-serif; font-size: 13px; font-weight: bold; margin-top: 0; margin-bottom: 0px; text-align: left; line-height:18px;">&nbsp; </h1>
<table width="272" cellspacing="0" cellpadding="0" border="0" style="line-height:18px; font-family:Arial, Helvetica, sans-serif; font-size:11px;">
                <tbody>
                <tr>
                <td>&nbsp; </td>
                </tr>
                <tr>
                <td width="209"><h4 style="margin:0px; font-weight:normal; text-transform:uppercase;"><span style="color:#666;font-weight:bold;margin-right:5px; text-transform:none;">Manufacturer: </span> {$printJob.ManufacturerName|escape:'html'|default:' -- '}</h4></td>
                </tr>
                <tr>
                <td><h4 style="margin:0px; font-weight:normal; text-transform:uppercase;"><span style="color:#666;font-weight:bold;margin-right:5px; text-transform:none;">Model: </span>{$printJob.ModelNumber|escape:'html'|default:' -- '}</h4></td>
                </tr>
                <tr>
                <td><h4 style="margin:0px; font-weight:normal; text-transform:uppercase;"><span style="color:#666;font-weight:bold;margin-right:5px; text-transform:none;">Unit Type: </span>{$printJob.UnitTypeName|escape:'html'|default:' -- '}</h4></td>
                </tr>
                <tr>
                <td><h4 style="margin:0px; font-weight:normal; text-transform:uppercase;"><span style="color:#666;font-weight:bold;margin-right:5px; text-transform:none;">Serial Number: </span>{$printJob.SerialNo|escape:'html'|default:' -- '}</h4></td>
                </tr>
                <tr>
                <td><h4 style="margin:0px; font-weight:normal; text-transform:uppercase;"><span style="color:#666;font-weight:bold;margin-right:5px; text-transform:none;">Accessories: </span>{$printJob.Accessories|escape:'html'|default:' -- '}</h4></td>
                </tr>
                <tr>
                <td><h4 style="margin:0px; font-weight:normal; text-transform:uppercase;"><span style="color:#666;font-weight:bold;margin-right:5px; text-transform:none;">Unit Condition: </span>{$printJob.UnitCondition|escape:'html'|default:' -- '}</h4>
                </td>
                </tr>
                <tr>
                <td><h4 style="margin:0px; font-weight:normal; text-transform:uppercase;"><span style="color:#666;font-weight:bold;margin-right:5px; text-transform:none;">Field Appointment: </span> -- </h4>
                </td>
                </tr>
                </tbody>
      		</table>
          </td>
  			</tr>
   			<tr>
      		<td height="10px" colspan="4">&nbsp;</td>
  			</tr>
  			<tr>
  			<td colspan="4"><!-- reported fault -->
  			<h1 align="left" style="color: #666666; font-family:Arial, Helvetica, sans-serif;line-height:18px; font-size: 14px; font-weight: bold; margin-top: 4; margin-bottom: 0px; text-align: center; text-transform:uppercase;width:130px; ">Reported Fault</h1>
  			<table bordercolor="#666666" cellpadding="0" cellspacing="0" width="100%" border-collapse="collapse">
  			<tbody>
                        
                        <tr><td style="border-top:1px solid #dddddd;"></td></tr>
 			<tr>
                            <td style="padding-left:5px;">
                                {$printJob.ReportedFault|escape:'html'}
                            </td>
                        </tr>
  			<tr><td style="border-top:1px solid #dddddd;">&nbsp; </td></tr>
                        
  			</tbody>
  			</table>
  		</td><!-- end of reported fault -->
 		</tr>
  		<tr>
    	<td class="odd"><h1 align="left" style="color: #039CDF; font-family:Arial, Helvetica, sans-serif;ine-height:18px; font-size: 16px; font-weight: bold; margin-top: 0; margin-bottom: 2px; text-align: left; text-transform:uppercase;">Service Provider</h1>
    	</td>
    	<td class="odd">&nbsp;</td>
    	<td class="odd">&nbsp;</td>
    	<td class="odd"><h1 align="left" style="color: #039CDF; font-family:Arial, Helvetica, sans-serif; font-size: 16px; font-weight: bold; margin-top: 0; margin-bottom: 0px; text-align: left; text-transform:uppercase;"></h1></td>
  		</tr>
  		<tr>
    	<td>
    	<table width="272" border="0" cellpadding="0" cellspacing="0" style="line-height:18px; font-family:Arial, Helvetica, sans-serif; font-size:11px;">
         <tbody>
           <tr>
              <td width="209"><h4 style="margin:0px; font-weight:bold; text-transform:uppercase; color:#666666;">{$printJob.ServiceCentreName|escape:'html'}</h4></td>
           </tr>
           <tr>
              <td><h4 style="margin:0px; font-weight:normal;">{$printJob.SCDetails1|default:'&nbsp;'}</h4></td>
            </tr>
           <tr>
              <td><h4 style="margin:0px; font-weight:normal;">{$printJob.SCDetails2|default:'&nbsp;'}</h4></td>
           </tr>
           <tr>
            <td><h4 style="margin:0px; font-weight:normal;">{$printJob.SCDetails3|default:'&nbsp;'}</h4></td>
           </tr>
           <tr>
              <td><h4 style="margin:0px; font-weight:normal;">{$printJob.SCDetails4|default:'&nbsp;'}</h4></td>
           </tr>
           <tr>
             <td><h4 style="margin:0px; font-weight:normal;">{$printJob.SCDetails5|default:'&nbsp;'}</h4></td>
           </tr>
           <tr>
             <td><h4 style="margin:0px; font-weight:normal;">{$printJob.SCDetails6|default:'&nbsp;'}</h4></td>
           </tr>
            <tr>
               <td >&nbsp;</td>
            </tr>
           </tbody>
      	</table>
	    </td>
			  <td >&nbsp;</td>
              <td >&nbsp;</td>
              <td >
               <table width="272" border="0" cellpadding="0" cellspacing="0" style="line-height:18px; font-family:Arial, Helvetica, sans-serif; font-size:11px;">
                <tbody>
                  <tr>
                    <td width="209"><h4 style="margin:0px; font-weight:normal;text-transform:uppercase;"><span style="color:#666;font-weight:bold;margin-right:5px; text-transform:none;">Job Type: </span>{$printJob.ServiceJobTypeName|default:'--'|escape:escape:'html'}</h4></td>
                  </tr>
                  <tr>
                    <td><h4 style="margin:0px; font-weight:normal;text-transform:uppercase;"><span style="color:#666;font-weight:bold;margin-right:5px;text-transform:none;">Job Site: </span>{$product_location|default:'--'|escape:escape:'html'}</h4></td>
                  </tr>
                  <tr>
                    <td><h4 style="margin:0px; font-weight:normal;text-transform:uppercase;"><span style="color:#666;font-weight:bold;margin-right:5px;text-transform:none;">Trade Account: </span>{$printJob.Client|default:'--'|escape:escape:'html'}</h4></td>
                  </tr>
                  <tr>
                    <td><h4 style="margin:0px; font-weight:normal;text-transform:uppercase;"><span style="color:#666;font-weight:bold;margin-right:5px;text-transform:none;">Referal Number: </span>{$printJob.AgentRefNo|default:'--'|escape:escape:'html'}</h4></td>
                  </tr>
                  <tr>
                    <td><h4 style="margin:0px; font-weight:normal;text-transform:uppercase;"><span style="color:#666;font-weight:bold;margin-right:5px;text-transform:none;">Engineer: </span>{$printJob.EngineerName|escape:'html'|default:' -- '}</h4></td>
                  </tr>
                  <tr>
                    <td><h4 style="margin:0px; font-weight:normal;text-transform:uppercase;"><span style="color:#666;font-weight:bold;margin-right:5px;text-transform:none;">Condition Code: </span>{$printJob.ConditionCode|escape:'html'|default:' -- '}</h4>
                    </td>
                  </tr>
                  <tr>
                     <td><h4 style="margin:0px; font-weight:normal;text-transform:uppercase;"><span style="color:#666;font-weight:bold;margin-right:5px;text-transform:none;">Symptom Code:</span>{$printJob.SymptomCode|escape:'html'|default:' -- '}</h4>
                     </td>
                   </tr>
                   <tr>
                      <td><h4 style="margin:0px; font-weight:normal;text-transform:uppercase;"><span style="color:#666;font-weight:bold;margin-right:5px;text-transform:none;">Date Completed: </span>{$printJob.ClosedDate|default:'--'|escape:escape:'html'}</h4>
                      </td>
                     </tr>
                   </tbody>
      			</table>
		  </tr>
  				<tr>
    				<td height="10px" colspan="4">
					&nbsp;													
    				</td>
  				</tr>
 				<tr style="padding-bottom:10px;">
 				 <td colspan="4"><!-- reported fault -->
  				  <h1 align="left" style="color: #666666; font-family:Arial, Helvetica, sans-serif;line-height:18px; font-size: 14px; font-weight: bold; margin-top: 4; margin-bottom: 4px; text-align: center; text-transform:uppercase;width:130px; ">Parts Required</h1>
				   <table width="600" cellspacing="0" cellpadding="0" bordercolor="#666666" style="line-height:18px; font-family:Arial, Helvetica, sans-serif; font-size:11px;border-collapse:collapse;">
  					<tr style="background:#ddd">
  						<td width="35" style="border:1px solid #dddddd;color:#666;font-weight:bold;margin-right:5px;text-transform:none;text-align:center;">Qty </td><td width="200" style="border:1px solid #dddddd;color:#666;font-weight:bold;margin-right:5px;text-transform:none;text-align:center;">Description </td>
  						<td style="border:1px solid #dddddd;color:#666;font-weight:bold;margin-right:5px;text-transform:none;text-align:center;">Part Number</td><td width="25" style="border:1px solid #dddddd;color:#666;font-weight:bold;margin-right:5px;text-transform:none; text-align:center;">S </td>
  						<td width="25" style="border:1px solid #dddddd;color:#666;font-weight:bold;margin-right:5px;text-transform:none;text-align:center;">D </td><td width="25" style="border:1px solid #dddddd;color:#666;font-weight:bold;margin-right:5px;text-transform:none;text-align:center;">R </td>
  						<td style="border:1px solid #dddddd;color:#666;font-weight:bold;margin-right:5px;text-transform:none;text-align:center;">Unit Cost </td><td style="border:1px solid #dddddd;color:#666;font-weight:bold;margin-right:5px;text-transform:none;text-align:center;">Line Cost </td>
  					</tr>
 	 				<tr>
  						<td style="border:1px solid #dddddd;text-align:center;">&nbsp;</td>
  						<td style="border:1px solid #dddddd;text-align:center;">&nbsp;</td>
  						<td style="border:1px solid #dddddd;text-align:center;">&nbsp;</td><td style="border:1px solid #dddddd;text-align:center;">&nbsp; </td>
  						<td style="border:1px solid #dddddd;text-align:center;">&nbsp; </td><td style="border:1px solid #dddddd;text-align:center;">&nbsp; </td>
  						<td style="border:1px solid #dddddd;text-align:center;">&nbsp;</td>
  						<td style="border:1px solid #dddddd;text-align:center;">&nbsp;</td>
  					</tr>
  					<tr>
  						<td style="border:1px solid #dddddd;text-align:center;">&nbsp; </td><td style="border:1px solid #dddddd;text-align:center;">&nbsp; </td>
  						<td style="border:1px solid #dddddd;text-align:center;">&nbsp; </td><td style="border:1px solid #dddddd;text-align:center;">&nbsp; </td>
  						<td style="border:1px solid #dddddd;text-align:center;">&nbsp; </td><td style="border:1px solid #dddddd;text-align:center;">&nbsp; </td>
  						<td style="border:1px solid #dddddd;text-align:center;">&nbsp; </td><td style="border:1px solid #dddddd;text-align:center;">&nbsp; </td>
  					</tr>
  					<tr>
  						<td style="border:1px solid #dddddd;text-align:center;">&nbsp; </td><td style="border:1px solid #dddddd;text-align:center;">&nbsp; </td>
  						<td style="border:1px solid #dddddd;text-align:center;">&nbsp; </td><td style="border:1px solid #dddddd;text-align:center;">&nbsp; </td>
  						<td style="border:1px solid #dddddd;text-align:center;">&nbsp; </td><td style="border:1px solid #dddddd;text-align:center;">&nbsp; </td>
  						<td style="border:1px solid #dddddd;text-align:center;">&nbsp; </td><td style="border:1px solid #dddddd;text-align:center;">&nbsp; </td>
  					</tr>
  				</table>
  			</td><!-- end of reported fault -->
  			</tr>
  			<tr>
  				<td colspan="4"><!-- reported fault -->
  				<h1 align="left" style="color: #666666; font-family:Arial, Helvetica, sans-serif;line-height:18px; font-size: 14px; font-weight: bold; margin-top: 4; margin-bottom: 0px; text-align: center; text-transform:uppercase;width:130px; ">service report</h1>
 				 <table bordercolor="#666666" cellpadding="0" cellspacing="0" width="100%" border-collapse="collapse">
  			<tr>
  				<td style="border-top:1px solid #dddddd;">&nbsp; </td>
  			</tr>
                                     
                                     
  			<tr  >
    			<td style="border-top:1px solid #dddddd;" > &nbsp; </td>
  			</tr>
                        
  			<tr>
    			<td style="border-top:1px solid #dddddd;">&nbsp; </td>
    		</tr>
  				</table>
  			</td><!-- end of reported fault -->
  		</tr>
    	<tr>
    		<td class="odd"><h1 align="left" style="color: #039CDF; font-family:Arial, Helvetica, sans-serif;ine-height:18px; font-size: 16px; font-weight: bold; margin-top: 0; margin-bottom: 0px; text-align: left; text-transform:uppercase;">customer details</h1>
    		</td>
    		<td class="odd">&nbsp;</td>
    		<td class="odd">&nbsp;</td>
    		<td class="odd">&nbsp;</td>
  		</tr>
  		<tr>
    		<td>
			<table width="272" border="0" cellpadding="0" cellspacing="0" style="line-height:18px; font-family:Arial, Helvetica, sans-serif; font-size:11px;">
              <tbody>
                <tr>
                  <td width="209"><h4 style="margin:0px; font-weight:bold; color:#666666;">{$consumerFullName|default:'--'|escape:escape:'html'}</h4></td>
                </tr>
                <tr>
                  <td><h4 style="margin:0px; font-weight:normal;">{$printJob.CDetails1|default:'&nbsp;'}</h4></td>
                </tr>
                <tr>
                  <td><h4 style="margin:0px; font-weight:normal;">{$printJob.CDetails2|default:'&nbsp;'}</h4></td>
                </tr>
                <tr>
                  <td><h4 style="margin:0px; font-weight:normal;">{$printJob.CDetails3|default:'&nbsp;'}</h4></td>
                </tr>
                 <tr>
                  <td><h4 style="margin:0px; font-weight:normal;">{$printJob.CDetails4|default:'&nbsp;'}</h4></td>
                 </tr>
                 <tr>
                   <td><h4 style="margin:0px; font-weight:normal;">{$printJob.CDetails5|default:'&nbsp;'}</h4></td>
                </tr>
                  <tr>
                    <td><h4 style="margin:0px; font-weight:normal;">{$printJob.CDetails6|default:'&nbsp;'}</h4></td>
                  </tr>
                  <tr>
                  <td ><h4 style="margin:0px; font-weight:normal;">{$printJob.CDetails7|default:'&nbsp;'}</h4></td>
                  </tr>
              </tbody>
      		</table>
			</td>
			<td >&nbsp;</td>
            <td >&nbsp;</td>
            <td ><table width="202" border="0" cellpadding="0" cellspacing="0" style="line-height:18px; font-family:Arial, Helvetica, sans-serif; font-size:11px;">
            <tbody>
              <tr>
              <td colspan="2">
              <table width="202" border="0" cellpadding="0" cellspacing="0" style="line-height:18px; font-family:Arial, Helvetica, sans-serif; font-size:11px; padding:5px;border:1px solid #ddd;">
                <tbody>
                  <tr>
                    <td colspan="2" width="150"></td>
                  </tr>
                  <tr>
                    <td colspan="2"><h5 align="left" style="color: #039CDF; font-family:Arial, Helvetica, sans-serif; font-size: 13px; font-weight: bold; margin-top: 0; margin-bottom: 0px; text-align: left; text-transform:uppercase;">Billing Details</h5></td>
                  </tr>
                  <tr>
                    <td><span style="color:#666;font-weight:bold;margin-right:5px;text-transform:none;">Labour:</span></td>
                    <td colspan="2"><h4 style="margin:0px; font-weight:normal;text-transform:uppercase; text-align:right;">{$InvoiceCosts.Labour}</h4></td>
                  </tr>
                  <tr>
                    <td><h4 style="margin:0px; font-weight:normal;text-transform:uppercase;"><span style="color:#666;font-weight:bold;margin-right:5px;text-transform:none;">Parts:</span></h4></td>
                    <td colspan="2"><h4 style="margin:0px; font-weight:normal;text-transform:uppercase; text-align:right;">{$InvoiceCosts.Parts}</h4></td>
                  </tr>
                  <tr>
                    <td><h4 style="margin:0px; font-weight:normal;text-transform:uppercase;"><span style="color:#666;font-weight:bold;margin-right:5px;text-transform:none;">Carriage</span></h4></td>
                    <td colspan="2"><h4 style="margin:0px; font-weight:normal;text-transform:uppercase; text-align:right;">{$InvoiceCosts.Carriage}</h4></td>
                  </tr>
                  <tr>
                    <td><h4 style="margin:0px; font-weight:normal;text-transform:uppercase;"><span style="color:#666;font-weight:bold;margin-right:5px;text-transform:none;">VAT @ {$InvoiceCosts.VATRate}%:</span></h4></td>
                    <td colspan="2"><h4 style="margin:0px; font-weight:normal;text-transform:uppercase; text-align:right;">{$InvoiceCosts.VAT}</h4></td>
                  </tr>
                  <tr>
                    <td colspan="3">&nbsp;</td>
                  </tr>
                  <tr>
                    <td><h2 style="margin:0px; font-weight:normal;text-transform:uppercase;"><span style="color:#666;font-weight:bold;margin-right:5px;text-transform:none;">Total:</span></h2></td>
                    <td><h2 style="margin:0px; font-weight:normal;text-transform:uppercase; text-align:right; color:#666;">£{$InvoiceCosts.Gross}</h2></td>
                  </tr>
                </tbody>
              </table>                <h5 align="left" style="color: #039CDF; font-family:Arial, Helvetica, sans-serif; font-size: 13px; font-weight: bold; margin-top: 0; margin-bottom: 0px; text-align: left; text-transform:uppercase;">&nbsp;</h5></td>
              </tr>
            </tbody>
            </table>
          </tr>
  </table>
</div>
</body>

</html>
		  
<script type="text/javascript" >

    $(document).ready(function() {    

	window.print();

    });    

</script>