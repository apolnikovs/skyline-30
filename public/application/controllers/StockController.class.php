<?php
/**
 * Description
 *
 * This class handles all actions of StockControll module
 *
 * @author      Andris Polnikovs <a.polnikovs@gmail.com>
 * @version     1.00
 *   
 * Changes
 * Date        Version Author                Reason
 * 30/04/2013  1.00    Andris Polnikovs      Initial Version
 * 
 ******************************************************************************/
require_once('CustomSmartyController.class.php');
include_once ('SkylineRESTClient.class.php');

class StockController extends CustomSmartyController {
    
  public  $config;  
    public  $session;
    public  $skyline;
    public  $user;
    public  $messages;
    private $lang = 'en';   
    public $debug = true;
    
    
    public function __construct() { 
       parent::__construct(); 
        
      
        
       /* ==========================================
        * Read Application Config file.
        * ==========================================
        */
        $this->config = $this->readConfig('application.ini');
        
       /* ==========================================
        *  Initialise Session Model
        * ==========================================
        */
        $this->session = $this->loadModel('Session'); 
        
        /* ==========================================
         *  Initialise Messages Model
         * ==========================================
         */
        $this->messages = $this->loadModel('Messages'); 
        
        /* =========== smarty assign =============== */
        $this->smarty->assign('ref', '');
        $this->smarty->assign('Search', '');
        $this->smarty->assign('ErrorMsg', '');
       
        
        
        
        /* ==========================================
         *  Initialise User Class
         * ==========================================
         */
        
         $user_model = $this->loadModel('Users');
            $this->user = $user_model->GetUser( $this->session->UserID,true);
            if(isset($_GET['name'])&&$_GET['password']&&isset($_GET['type'])){
               if ($this->debug) $this->log($_GET,'SB_access_log_');
               foreach($_GET as $k=>$r){
                    if($k!="name"&&$k!="password"){
                       
                   
                   $_GET[$k]=preg_replace('/[^a-zA-Z0-9_ %\[\]\.\(\)%&-]/s', '', $r);
                   }
               }
               if ($this->debug) $this->log('--Update remove','SB_access_log_');
               if ($this->debug) $this->log($_GET,'SB_access_log_');
           }
        if(isset($this->session->UserID)) {

            $user_model = $this->loadModel('Users');
            $this->user = $user_model->GetUser( $this->session->UserID );
            
	    $this->smarty->assign('loggedin_user', $this->user);
	    
            $this->smarty->assign('name',$this->user->Name);
            $this->smarty->assign('session_user_id', $this->session->UserID);
            $this->smarty->assign('_theme', $this->user->Skin);
            
            if($this->user->BranchName)
            {
                $this->smarty->assign('logged_in_branch_name', " - ".$this->user->BranchName." ".$this->config['General']['Branch']." ");
            }
            else
            {
                $this->smarty->assign('logged_in_branch_name', "");
            }
            
            if($this->session->LastLoggedIn)
            {
                $this->smarty->assign('last_logged_in', " - ".$this->config['General']['LastLoggedin']." ".date("jS F Y G:i", $this->session->LastLoggedIn));
            }
            else
            {
                $this->smarty->assign('last_logged_in', '');
            } 
            
            $topLogoBrandID = $this->user->DefaultBrandID;
            
            //$this->log(var_export($this->user, true));
            

        } else {

            $topLogoBrandID = isset($_COOKIE['brand'])?$_COOKIE['brand']:0;
            
            $this->smarty->assign('session_user_id','');
            $this->smarty->assign('_theme', 'skyline');
           // $this->smarty->assign('name','');
           // $this->smarty->assign('last_logged_in','');
           // $this->smarty->assign('logged_in_branch_name', '');
            $this->redirect('index',null, null);
            

        } 
     
     }
    
    public function indexAction($args){
        $this->redirect('StockController','defaultAction');
    }
    public function stockAction($args){
        $this->redirect('StockController','defaultAction');
    }
    
    public function defaultAction($args){
        
      
        ///session
        $this->session->mainTable="sp_part_stock_template";
        $this->session->mainPage="stock";
        $this->session->controller="Stock";
        $this->page =  $this->messages->getPage("stock", $this->lang);
        //session
        
         $this->checkUserSPID($args);
        
        //models
        $StockModel= $this->loadModel('Stock');
        $datatable= $this->loadModel('DataTable');
        //models
         
         
       
      
       
        $keys_data=$datatable->getAllFieldNames($this->session->mainTable,$this->user->UserID,$this->page['config']['pageID']);
        if(isset($keys_data[1])){
        $keys=$keys_data[1];
        }else{
        $keys=array();
        }
  
       
         
      
        if($this->user->SuperAdmin==1){
          $ServiceProviders = $this->loadModel('ServiceProviders');
        $splist=$ServiceProviders->getAllActiveServiceProviders();
         $this->smarty->assign('splist', $splist);
        }
          ///smarty assign
        $this->smarty->assign('data_keys', $keys);
        $this->smarty->assign('SuperAdmin', $this->user->SuperAdmin);
        $this->smarty->assign('ServiceProviderID', $this->user->ServiceProviderID);
        $this->smarty->assign('page', $this->page);
        
        ///smarty assign
         
         
         $this->smarty->display('stock/stockMain.tpl');
    }
    
    
    
    
    
    public function tableDisplayPreferenceSetupAction($args){
         $this->page =  $this->messages->getPage($args['page'], $this->lang);
        $this->smarty->assign('page', $this->page);
        $this->smarty->assign('SupderAdmin', $this->user->SuperAdmin);//this value should be from user calss
   
        $datatable= $this->loadModel('DataTable');
        
        $columnStrings=$datatable->getColumnStrings($this->user->UserID,$this->page['config']['pageID'],'user');
        $columnStringsSA=$datatable->getColumnStrings(1,$this->page['config']['pageID'],'sa');
        
        
       $keys=$datatable->getAllFieldNames($args['table']);
       
       if(isset($keys[1][0])){
        $this->smarty->assign('data_keys', $keys[1]);
       }else{
           $this->smarty->assign('data_keys', $keys);  
       }
      
       
       
        if($columnStrings){
        $columnDisplayString=  explode(",", $columnStrings['ColumnDisplayString']);
        $columnOrderString=explode(',',$columnStrings['ColumnOrderString']);
        $columnNameString=explode(",", $columnStrings['ColumnNameString']);
      
        
        }else{
           $columnDisplayString=array(); 
           $columnOrderString="";
           $columnNameString=array();
           
        }
        
         if($columnStringsSA){
        $columnDisplayStringSA=  explode(",", $columnStringsSA['ColumnDisplayString']);
        $columnOrderStringSA=explode(',',$columnStringsSA['ColumnOrderString']);
        $columnNameStringSA=explode(",", $columnStringsSA['ColumnNameString']);
        $columnStatusStringSA=explode(",", $columnStringsSA['ColumnStatusString']);
         $this->smarty->assign('columnStatusStringSA', $columnStatusStringSA);
        
        }else{
           $columnDisplayStringSA=array(); 
           $columnOrderStringSA="";
           $columnNameStringSA=array();
           
        }
       
        $this->smarty->assign('columnDisplayString', $columnDisplayString);
        $this->smarty->assign('columnOrderString', $columnOrderString);
        $this->smarty->assign('columnNameString', $columnNameString);
        $this->smarty->assign('columnDisplayStringSA', $columnDisplayStringSA);
        $this->smarty->assign('columnOrderStringSA', $columnOrderStringSA);
        $this->smarty->assign('columnNameStringSA', $columnNameStringSA);
       $this->smarty->assign('controller', $this->session->controller);
     
        $this->smarty->assign('typeAction', $this->session->mainPage);
       
         $this->smarty->display('systemAdmin/tableDisplayPreferences.tpl');  
    }
    
    public function saveDisplayPreferencesAction($args){
       $typeAction=$args['typeAction'];
         $this->page =  $this->messages->getPage($this->session->mainPage, $this->lang);
          $model= $this->loadModel('DataTable');
       
        $model->saveDisplayPreferences($this->user->UserID,$this->page['config']['pageID'],$_POST);
         $this->redirect($this->session->controller.'Controller',$typeAction."Action");
    }
    
    public function processStockAction($args){
        
     $models_model= $this->loadModel('Models');
     $colour_model= $this->loadModel('Colours');
         $suppliers_model= $this->loadModel('Suppliers');
        $suppliers=$suppliers_model->getSPSuppliers($this->user->ServiceProviderID);
        $this->smarty->assign('suppliers', $suppliers);
        $this->page =  $this->messages->getPage($this->session->mainPage, $this->lang);
         if($this->user->ServiceProviderID==""){
            die($this->page['Errors']['no_service_provider_id_error']);
        }
         $this->smarty->assign('page', $this->page);
        
         
         $this->smarty->assign('ServiceProviderID', $this->user->ServiceProviderID);
        
         
         $models=$models_model->getAllSPModels($this->user->ServiceProviderID);
         $colours=$colour_model->getAllSPColours($this->user->ServiceProviderID);
         
           $this->smarty->assign('models', $models);
             $this->smarty->assign('colours', $colours);
         //edit,copy
         if(isset($args['id'])){
               $model= $this->loadModel('Stock');
             
                 $this->smarty->assign('mode', "update");
                 
                 $datarow=$model->getData($args['id'],$this->session->mainTable);
                 
                 $this->smarty->assign('datarow', $datarow);
             $this->smarty->display('stock/stockPartForm.tpl');
         }else{
            
             $datarow=[
                 "Status"=>"Active",
                 "PartNumber"=>"",
                 "PurchaseCost"=>"",
                 "Description"=>"",
                 "MinStock"=>"",
                 "MakeUpTo"=>"",
                 "ServiceProviderColourID"=>"",
                 "PrimaryServiceProviderModelID"=>"",
                 "DefaultServiceProviderSupplierID"=>"",
             ];
             
                   
                      $this->smarty->assign('mode', "New");
                      $this->smarty->assign('datarow', $datarow);
                    
                  
              $this->smarty->display('stock/stockPartForm.tpl');
             }
         
         
         
       
    }
    
    
    public function savePartStockTemplateAction($args){
       
   
     $model= $this->loadModel('Stock');
     if($_POST['SpPartStockTemplateID']!=''){
       $model->updatePartStockTemplate($_POST,$this->user->ServiceProviderID);
        }else{
       $model->insertPartStockTemplate($_POST,$this->user->ServiceProviderID);
        
        }
        $this->redirect($this->session->controller);
      
}
    public function deleteStockAction($args){
      $model= $this->loadModel('Stock');
      $model->deleteStock($args['id']);
      $this->redirect($this->session->controller);
        
    }
    
    public function resetDisplayPreferencesAction($args)
    {
         $typeAction=$args['typeAction'];
         
         $this->page =  $this->messages->getPage( $typeAction, $this->lang);
          $model= $this->loadModel('DataTable');
          
          $model->resetDisplayPreferences($this->user->UserID,$this->page['config']['pageID']);
           $this->redirect($this->session->controller.'Controller',$typeAction."Action");
    }
    
    
    public function loadStockTableAction($args)
            {
     
       $dd=$_GET;
      $this->page =  $this->messages->getPage($this->session->mainPage, $this->lang);
        $datatable= $this->loadModel('DataTable');
        if(isset($args['inactive'])){
           $inactive=true;
       }else{
           $inactive=false;
       }
       $table=$this->session->mainTable;
       if($this->user->ServiceProviderID!=""){
           $sp=$this->user->ServiceProviderID;
         
       }else{
           if($this->user->SuperAdmin==1&&isset($args['spid']))
           {
              $sp= $args['spid'];
           }else{
           $sp=false;
           }
       }
      
       
       
        $keys_data=$datatable->getAllFieldNames($table,$this->user->UserID,$this->page['config']['pageID']);
        if(isset($keys_data[0])){
        $columns=$keys_data[0];
        }else{
          $columns=$keys_data;  
        }
       
      
     // $columns=$datatable->getAllFieldNames('currency');
       //$columns="";
    
       
       $joins="join service_provider sp On sp.ServiceProviderID= $table.ServiceProviderID
              
              left join user u on u.UserID=$table.CreatedUserID
              left join user u2 on u2.UserID=$table.ModifiedUserID
               left join service_provider_colour c on c.ServiceProviderColourID=$table.ServiceProviderColourID
              left join service_provider_supplier sup on sup.ServiceProviderSupplierID=$table.DefaultServiceProviderSupplierID
               
                ";
       //$joins="";
       
        if($this->debug) $this->log("start","datatableData");
        $data=$datatable->datatableSS($table,$columns,$dd,$joins,$extraColumns=array('<input type="checkbox">'),0,false,$inactive,$sp);
      if($this->debug) $this->log($data,"datatableData");
        echo json_encode($data);
    }
    
    
    public function stockPartHistoryAction($args){
        $id=$args['id'];
        $model= $this->loadModel('Stock');
        $templateData=$model->getData($id,$this->session->mainTable);
        
        
            
            $this->smarty->assign("Data",$templateData);
            $this->smarty->assign("id",$id);
              $this->smarty->display('stock/stockPartHistory.tpl');
    }
    //data table serverside function
    
    public function loadStockHistoryListAction($args){
        $dd=$_GET;
      $this->page =  $this->messages->getPage($this->session->mainPage, $this->lang);
        $datatable= $this->loadModel('DataTable');
       
       
      $id=$args['id'];
    
       $columns=["date_format(DateTime,'%d/%m/%Y (%H:%i)')","ps.PartStatusName","count(*)","Reference"];
    
       $t="sp_part_stock_history";
       
       $joins="
               join part_status ps on ps.PartStatusID=$t.SPPartStatusID 
               join sp_part_stock_item spsi on spsi.SPPartStockItem=$t.SPPartStockItemID 
                ";
      $groupBy="group by date_format(DateTime,'%d/%m/%Y (%H)'), Reference, $t.SPPartStatusID";
       
      $where=" and spsi.SPPartStockTemplateID=$id ";
        $data=$datatable->datatableSS($t,$columns,$dd,$joins,array(),0,false,'no',false,"",$groupBy,$where,true);
     // $this->log($data,"datatableData");
        echo json_encode($data);
    }
    
    public function stockReiceivingWindowAction($args){
        $this->page =  $this->messages->getPage("stockReceiving", $this->lang);
        if($this->user->ServiceProviderID==""){
            die($this->page['Errors']['no_service_provider_id_error']);
        }
        $suppliers_model= $this->loadModel('Suppliers');
        $suppliers=$suppliers_model->getSPSuppliers($this->user->ServiceProviderID);
         $models_model= $this->loadModel('Models');
        $colour_model= $this->loadModel('Colours');
        $stock_model= $this->loadModel('Stock');
       
        
        
          $stockTemplates=$stock_model->getSpPartTemplates($this->user->ServiceProviderID);
         $models=$models_model->getAllSPModels($this->user->ServiceProviderID);
         $colours=$colour_model->getAllSPColours($this->user->ServiceProviderID);
          
        
        
         
          $this->smarty->assign('page', $this->page);
          $this->smarty->assign('stockTemplates', $stockTemplates);
          $this->smarty->assign('models', $models);
         $this->smarty->assign("suppliers",$suppliers);
         $this->smarty->assign("colours",$colours);
        $this->smarty->display('stock/stockReceivingWindow.tpl');
    }
    //data table function
    public function loadStockReceivingHistoryListAction($args){
       
         $dd=$_GET;
    
        $datatable= $this->loadModel('DataTable');
       
      
       if($this->user->ServiceProviderID!=""){
           $sp=$this->user->ServiceProviderID;
         
       }else{
           $sp="0";
          
       }
      
       
       
      $t="sp_part_order";
      
  
       $columns=array("SPPartOrderID","date_format(ReceviedDate,'%d/%m/%Y %H:%i')","OrderNo","CompanyName","concat_ws(' ',u.ContactFirstName,u.ContactLastName)");
    
       
       $joins="join user u on u.UserID=$t.ReceivedByUserID
               left join service_provider_supplier sps on sps.ServiceProviderSupplierID=$t.ServiceProviderSupplierID";
       //$joins="";
       
        if($this->debug) $this->log("start","datatableData");
        $data=$datatable->datatableSS($t,$columns,$dd,$joins,$extraColumns=array('<input type="checkbox">'),0,false,"",$sp);
      if($this->debug) $this->log($data,"datatableData");
        echo json_encode($data);
    }
    
    
    public function checkOrderNoAction($args){
        $model= $this->loadModel('Stock');
        $res=$model->checkOrderNo($_POST['orderNo'],$this->user->ServiceProviderID);
           if($res){echo json_encode($res);}else{echo "";} 
      }
      
      
    public function getSpPartTemplateDataAction($args){
        $model= $this->loadModel('Stock');
        $res=$model->getSpPartTemplateData($this->user->ServiceProviderID,$_POST['PartNumber']);
        echo json_encode($res);
    }
    
    
    public function insertStockItemAction(){
      
       
        
         $model= $this->loadModel('Stock');
          //getting order data
         $on=$model->checkOrderNo($_POST['OrderNo'],$this->user->ServiceProviderID);
         if($on['OrderedDate']==""){
         $model->insertStockItem($this->user->ServiceProviderID,$_POST);
         }else{
         $model->receiveGSPNStockItem($_POST,$on);    
         }
        
    }
    
    //job update page ataching parts to job
    public function loadSPPartsListAction($args){
          
        $datatable= $this->loadModel('DataTable');
         $table="sp_part_stock_template";
        
        $columns=["SpPartStockTemplateID",
            "PartNumber",
            "Description",
            "(select count(*) from sp_part_stock_item sppsi 
                join part_status sps on sps.PartStatusID=sppsi.SPPartStatusID
                where 
                sppsi.SPPartStockTemplateID=$table.SPPartStockTemplateID
                and sps.Available='Y' and sps.InStock='Y'    
                )"];  
        
       $joins="";
      
       $sp=$this->user->ServiceProviderID;
       //$joins="";
       
        if($this->debug) $this->log("start","datatableData");
        $data=$datatable->datatableSS($table,$columns,$_GET,$joins,$extraColumns=array('<input onclick="useThisItem(this)" type="checkbox">'),0,false,false,$sp);
      if($this->debug) $this->log($data,"datatableData");
        echo json_encode($data);
    }
    
    public function getStockItemDataAction($args){
         $model= $this->loadModel('Stock');
         $datarow=$model->getData($args['id'],"sp_part_stock_template");
         echo json_encode($datarow);
    }
    
    public function getOrderHistoryDataAction($args){
        
        $model= $this->loadModel('Stock');
         $datarow=$model->getOrderHistoryData($_POST['id'],$this->user->ServiceProviderID);
         echo json_encode($datarow);
    }
    
   
    
    public function  loadOrderReceivingHistoryListAction($args){
       
         $dd=$_GET;
    
        $datatable= $this->loadModel('DataTable');
       
      
       if($this->user->ServiceProviderID!=""){
           $sp=$this->user->ServiceProviderID;
         
       }else{
           $sp="0";
          
       }
      
       
       
      $t="sp_part_order";
      $orderID=$args['id'];
  
       $columns=["distinct(spst.PartNumber)","spst.Description","(select count(*) from sp_part_stock_item spsi2 where spsi2.SPPartOrderID=$orderID and spsi2.SPPartStockTemplateID=spst.SPPartStockTemplateID)"];
    
       
       $joins=" join sp_part_stock_item spsi on spsi.SPPartOrderID=$t.SPPartOrderID and $t.SPPartOrderID=$orderID
                join sp_part_stock_template spst on spst.SpPartStockTemplateID=spsi.SPPartStockTemplateID";
       //$joins="";
       
        if($this->debug) $this->log("start","datatableData");
        $data=$datatable->datatableSS($t,$columns,$dd,$joins,$extraColumns=array(),0,false,"",$sp);
      if($this->debug) $this->log($data,"datatableData");
        echo json_encode($data);
    }
    
    
    public function minimumStockRoutineAction()
    {
         $this->page =  $this->messages->getPage("stock", $this->lang);
        if($this->user->ServiceProviderID==""){
            die($this->page['Errors']['no_service_provider_id_error']);
        }
         $this->smarty->display('stock/stockMinimumStockRoutine.tpl');
    }
    
    
    public function minimumStockRoutineExportAction($args){
        
        $model= $this->loadModel('Stock'); 
       $type=$args['type'];
  $qr=$model->getExportData($type,$this->user->ServiceProviderID);
        header("Pragma: public");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Content-Type: application/force-download");
header("Content-Type: application/octet-stream");
header("Content-Type: application/download");
 
//this line is important its makes the file name
header("Content-Disposition: attachment;filename=skyline-stock-export-".date("d_m_Y").".csv ");
 
header("Content-Transfer-Encoding: binary ");
 
 
// these will be used for keeping things in order.
$col = 0;
$row = 0;
 
// This tells us that we are on the first row
$first = true;
$separator="	";
$separator=",";
$expa="";

if($type==1){
    $expa.="Minimum Stock Routine";
    $expa.= "\r\n";
    $expa.="Type All Stock";
    $expa.= "\r\n";
    $expa.="Created Date  ".date("d/m/Y");;
    $expa.= "\r\n";
    $expa.="Created By  ".$this->user->ContactFirstName." ".$this->user->ContactLastName;
    $expa.= "\r\n";
    $expa.= "\r\n";
}

if($type==2){
    $expa.="Minimum Stock Routine";
    $expa.= "\r\n";
    $expa.="Type Stock At Minimum Level Only";
    $expa.= "\r\n";
    $expa.="Created Date  ".date("d/m/Y");;
    $expa.= "\r\n";
    $expa.="Created By  ".$this->user->ContactFirstName." ".$this->user->ContactLastName;
    $expa.= "\r\n";
    $expa.= "\r\n";
}

while( $qrow = mysql_fetch_assoc( $qr ) )
{
    
    // Ok we are on the first row
    // lets make some headers of sorts
    if( $first )
    {
        
        foreach( $qrow as $k => $v )
        {
            $field =$k;
            // take the key and make label
            // make it uppper case and replace _ with ' '
          if (preg_match('/\\r|\\n|,|"/', $k)) {
            $field = '"' . str_replace('"', '""', $k) . '"';
            $field = '"' . str_replace('\r', '', $k) . '"';
            $field = '"' . str_replace('\n', '', $k) . '"';
           $field=preg_replace('/[^a-zA-Z0-9_ %\[\]\.\(\)%&-]/s', '', $field);
        }
        $expa.=  $field.$separator;
       if ($this->debug) $this->log($field.$separator,'export_CSV_log_');
            $col++;
        }
 
        // prepare for the first real data row
        $col = 0;
        $row++;
        $first = false;
         $expa.= "\r\n";
    }
     // go through the data
    foreach( $qrow as $k => $v )
    {
       $field =$v;
            // take the key and make label
            // make it uppper case and replace _ with ' '
          if (preg_match('/\\r|\\n|,|"/', $v)) {
             $field = '"' . str_replace('"', '""', $k) . '"';
            $field = '"' . str_replace('\r', '', $k) . '"';
            $field = '"' . str_replace('\n', '', $k) . '"';
           $field=preg_replace('/[^a-zA-Z0-9_ %\[\]\.\(\)%&-]/s', '', $field);
        }
        $expa.=  $field.$separator;
      
           
        
    }
  $expa.= "\r\n";
}
 if ($this->debug) $this->log($expa,'export_CSV_log_');
 echo $expa;
//$this->xlsEOF();
exit();
    }
    
    //cheks if user got service provider id
    private function checkUserSPID($args){
      //$this->print_d($this->user);
        if($this->user->ServiceProviderID==""){
            if($this->user->SuperAdmin==1){
                $this->smarty->assign("SuperAdmin",true);
            }else{
                
                $this->smarty->assign('NotPermited',true);
                $this->smarty->assign('type',"error");
                $this->smarty->assign('msgTitle',$this->page['Errors']['error']);
                $this->smarty->assign('msgText',$this->page['Errors']['no_service_provider_id_error']);
                $this->smarty->display("popup/UserMessage.tpl");
               return true;
            }
        }else{
            return false;
        }
    }
    
    
    public function getStockItemQtyAction($args){
         $model= $this->loadModel('Stock');
         $datarow=$model->getStockItemQty($args['id'],$this->user->ServiceProviderID);
         echo json_encode($datarow);
    }
    
    public function stockUsageReportAction($args){
        
         $this->page =  $this->messages->getPage("stock", $this->lang);
        if($this->user->ServiceProviderID==""){
            die($this->page['Errors']['no_service_provider_id_error']);
        }
         $this->smarty->display('stock/stockUsageReport.tpl');
    }
    
    public function StockUsageReportGenAction($args){
       $model= $this->loadModel('Stock'); 
       
  $qr=$model->getExportData(3,$this->user->ServiceProviderID,$_GET['dateFrom'],$_GET['dateTo']);
        header("Pragma: public");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Content-Type: application/force-download");
header("Content-Type: application/octet-stream");
header("Content-Type: application/download");
 
//this line is important its makes the file name
header("Content-Disposition: attachment;filename=skyline-stock-export-".date("d_m_Y").".csv ");
 
header("Content-Transfer-Encoding: binary ");
 
 
// these will be used for keeping things in order.
$col = 0;
$row = 0;
 
// This tells us that we are on the first row
$first = true;
$separator="	";
$separator=",";
$expa="";


    $expa.="Stock Usage Report";
    $expa.= "\r\n";
    $expa.="Date From: ".$_GET['dateFrom'];
    $expa.= "\r\n";
    $expa.="Date To: ".$_GET['dateTo'];
    $expa.= "\r\n";
    $expa.="Created Date  ".date("d/m/Y");;
    $expa.= "\r\n";
    $expa.="Created By  ".$this->user->ContactFirstName." ".$this->user->ContactLastName;
    $expa.= "\r\n";
    $expa.= "\r\n";


while( $qrow = mysql_fetch_assoc( $qr ) )
{
    
    // Ok we are on the first row
    // lets make some headers of sorts
    if( $first )
    {
        
        foreach( $qrow as $k => $v )
        {
            $field =$k;
            // take the key and make label
            // make it uppper case and replace _ with ' '
          if (preg_match('/\\r|\\n|,|"/', $k)) {
            $field = '"' . str_replace('"', '""', $k) . '"';
            $field = '"' . str_replace('\r', '', $k) . '"';
            $field = '"' . str_replace('\n', '', $k) . '"';
           $field=preg_replace('/[^a-zA-Z0-9_ %\[\]\.\(\)%&-]/s', '', $field);
        }
        $expa.=  $field.$separator;
       if ($this->debug) $this->log($field.$separator,'export_CSV_log_');
            $col++;
        }
 
        // prepare for the first real data row
        $col = 0;
        $row++;
        $first = false;
         $expa.= "\r\n";
    }
     // go through the data
    foreach( $qrow as $k => $v )
    {
       $field =$v;
            // take the key and make label
            // make it uppper case and replace _ with ' '
          if (preg_match('/\\r|\\n|,|"/', $v)) {
             $field = '"' . str_replace('"', '""', $k) . '"';
            $field = '"' . str_replace('\r', '', $k) . '"';
            $field = '"' . str_replace('\n', '', $k) . '"';
           $field=preg_replace('/[^a-zA-Z0-9_ %\[\]\.\(\)%&-]/s', '', $field);
        }
        $expa.=  $field.$separator;
      
           
        
    }
  $expa.= "\r\n";
}
 if ($this->debug) $this->log($expa,'export_CSV_log_');
 echo $expa;
//$this->xlsEOF();
exit();
    }
   
    
  public function stockOrderingAction($args){
       $datatable= $this->loadModel('DataTable');
      $this->session->mainTable="part";
        $this->session->mainPage="stockOrdering";
        $this->session->controller="Stock";
      
      $this->checkUserSPID($args);
        $this->page =  $this->messages->getPage("stockOrdering", $this->lang);
        $this->checkUserSPID($args);
         $suppliers_model= $this->loadModel('Suppliers');
        $suppliers=$suppliers_model->getSPSuppliers($this->user->ServiceProviderID);
        $keys_data=$datatable->getAllFieldNames("part",$this->user->UserID,$this->page['config']['pageID']);
   if(isset($keys_data[1])){
       $keys=$keys_data[1];
        }else{
         $keys=$columns=$keys_data;  
        }
         ///smarty assign
         $this->smarty->assign("suppliers",$suppliers);
        $this->smarty->assign('data_keys', $keys);
        $this->smarty->assign('SuperAdmin', $this->user->SuperAdmin);
        $this->smarty->assign('ServiceProviderID', $this->user->ServiceProviderID);
        $this->smarty->assign('page', $this->page);
        $stockModel=$model= $this->loadModel('Stock');
        $statuses=$stockModel->getOrderingStatuses(); 
         $this->smarty->assign('statuses', $statuses);
        ///smarty assign
         
         
         $this->smarty->display('stock/stockOrdering.tpl');
  }
  
  //data table serverside function
    
    public function loadStockOrderingTableAction($args){
        $dd=$_GET;
      $this->page =  $this->messages->getPage("stockOrdering", $this->lang);
        $datatable= $this->loadModel('DataTable');
       
       if($this->user->ServiceProviderID!=""){
           $sp=$this->user->ServiceProviderID;
         
       }else{
           $sp="0";
          
       }
    $t="part";
    
       
        $keys_data=$datatable->getAllFieldNames($t,$this->user->UserID,$this->page['config']['pageID']);
   if(isset($keys_data[0])){
        $columns=$keys_data[0];
        }else{
          $columns=$keys_data;  
        }
       $sp=false;
       
       $joins=" join sp_part_stock_template spst on spst.SpPartStockTemplateID=$t.SPPartStockTemplateID
                join service_provider_supplier sps on sps.ServiceProviderSupplierID = spst.DefaultServiceProviderSupplierID
                join part_status spsta on spsta.PartStatusID=$t.PartStatusID
                join part p on p.SpPartStockTemplateID=spst.SpPartStockTemplateID
                ";
      $groupBy="";
      
      $where="and $t.PartStatusID in (2,6,18) and spsta.PartStatusID in (2,6,18)";
      
       if(isset($args['suplierID'])&&$args['suplierID']!=0){
           $where.=" and sps.ServiceProviderSupplierID=".$args['suplierID'];
       }
      
      $groupBy="group by $t.PartID, $t.JobID";
        $data=$datatable->datatableSS($t,$columns,$dd,$joins,array('<input type="checkbox">'),0,false,'no',$sp,"",$groupBy,$where,true);
     // $this->log($data,"datatableData");
        echo json_encode($data);
    }
    
    
    public function changePartsStatusAction($args){
        $d=$_POST;
        if(!is_array($d['parts'])){$d['parts']=array($d['parts']);}
          $model= $this->loadModel('Stock');
          $model->changePartsStatus($d['parts'],$d['status']);
          echo"ok";
    }
  
    
    public function testAction(){
        $model= $this->loadModel('Stock');
        $model->insertStockItemHistory('2089',6,157751);
    }
    
   
    public function MakeTaggedOrdersAction($args){
        $model= $this->loadModel('Stock');
        if(isset($_POST['parts'])){
        $model->MakeTaggedOrders($_POST['parts']);
        }
    }
   
    
    public function addModelAction($args) {
        $model = $this->loadModel('Stock');
        $model->addModel($_POST['SpPartStockTemplateID'], $_POST['ACCID']);
    }

    public function loadModelAction($args) {
        $model = $this->loadModel('Stock');
        $data = $model->loadModel($_POST['SpPartStockTemplateID']);
        echo json_encode($data);
    }

    public function delModelAction($args) {
        $model = $this->loadModel('Stock');
        $model->delModel($_POST['SpPartStockTemplateID'], $_POST['ACCID']);
    }

    
}
  
?>
