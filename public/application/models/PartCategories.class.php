<?php

require_once('CustomModel.class.php');
require_once('TableFactory.class.php');

/**
 * Description
 *
 * This class is used for handling database actions of PartCategory Page in Lookup Tables section under System Admin
 *
 * @author      Andris Polnikovs <a.polnikovs@gmail.com>
 * @version     1.0 
 * @created     04/06/2013
 */
class PartCategories extends CustomModel {

    public function __construct($controller) {

        parent::__construct($controller);

        $this->conn = $this->Connect($this->controller->config['DataBase']['Conn'], $this->controller->config['DataBase']['Username'], $this->controller->config['DataBase']['Password']);
        $this->SQLGen = $this->controller->loadModel('SQLGenerator');
        $this->fields = [
            
            "CategoryName",
            "Status",
	    "ApproveStatus",
            "ServiceProviderID" 
           
            
        ];
    }

    public function insertPartCategory($P, $spid) {
        $P["ServiceProviderID"] = $spid;
        $P["Status"] = isset($P["Status"]) ? $P["Status"] : "Active";
	$P["ApproveStatus"] = isset($P["ApproveStatus"]) ? $P["ApproveStatus"] : "Approved";
        $id = $this->SQLGen->dbInsert('service_provider_part_category', $this->fields, $P, true, true);
        return $id;
    }

    public function updatePartCategory($P, $spid) {
        $P["ServiceProviderID"] = $spid;
        $P["Status"] = isset($P["Status"]) ? $P["Status"] : "Active";
	$P["ApproveStatus"] = isset($P["ApproveStatus"]) ? $P["ApproveStatus"] : "Approved";
        $id = $this->SQLGen->dbUpdate('service_provider_part_category', $this->fields, $P, "ServiceProviderPartCategoryID=" . $P['ServiceProviderPartCategoryID'], true);
    }

    public function getPartCategoryData($id) {
        $sql = "select * from service_provider_part_category where ServiceProviderPartCategoryID=$id";
        $res = $this->query($this->conn, $sql);
        return $res[0];
    }

    public function deletePartCategory($id) {
        $sql = "update service_provider_part_category set Status='In-Active' where ServiceProviderPartCategoryID=$id";
        $this->execute($this->conn, $sql);
    }
    
    public function getAllSPPartCategories($spid){
        $sql="select * from service_provider_part_category where ServiceProviderID=$spid and Status='Active'";
        $res = $this->query($this->conn, $sql);
        return $res;
    }
   
    

}

?>