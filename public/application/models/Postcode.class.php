<?php

require_once('CustomModel.class.php');

class Postcode extends CustomModel  {
    
    private $client;
    private $error;
    private $SkylineBrandID = 1000;
    
    public function __construct($controller) {
    
        parent::__construct($controller); 

        $this->error = '';

        $uri = "http://tempuri.org/";
       try {
        $this->client =
            new SoapClient($this->controller->config['Postcode']['URL'] . '?wsdl',
                            array('uri' =>  $uri, 'trace' => 1));
        } catch (Exception $e) {
            $this->controller->log(var_export($e->getMessage(), true));
            return false;
        }
    }
    
    public function getAddress($postcode) {
                 
        if(!$this->client)
        {
            return false;
        }
        $this->error = '';
        $params = new stdClass();

        try {
            
            $params->PrgLicNo = $this->controller->config['Postcode']['Licence'];;
            
            // split postcode into 2 parts (!!!)
            $pc = str_replace(' ','',$postcode);
            $len = strlen($pc);
        
            if ($len < 3) {
                $params->OutCode = $pc;
                $params->InCode = '';
            } else {
                $params->InCode = substr($pc,-3);
                $params->OutCode = substr($pc,0,$len-3);
            }
                      
            $params->Debug = 1;
            
            $response = $this->client->GetAddress($params);
                                           
            $result = explode( ';', $response->GetAddressResult );
            $address = array();
            
            
            //Loading County model
            $county_model = $this->controller->loadModel('County');
            if(isset($this->controller->user->DefaultBrandID))
            {
                $BrandID = $this->controller->user->DefaultBrandID;

            } 
            else 
            {

                $BrandID = isset($_COOKIE['brand'])?$_COOKIE['brand']:$this->SkylineBrandID;
            }
            $CountyID = 0;
            $CountryID = 0;            
            
            foreach($result as $elt) {
                if (strlen($elt) > 0) {
                    $arr = explode(',', $elt);
                    // sanity check for valid address record....
                    if (count($arr) == 11) {
                        
                        if(!$CountyID && isset($arr['8']) && $arr['8'])
                        {
                            
                            $CountyDetails = $county_model->getCountyIDFromPostCode($BrandID, $arr['8']);
                            
                            if(is_array($CountyDetails))
                            {
                              //  $CountyID  = $CountyDetails['Name'];
                                if(strtolower($CountyDetails['Name'])!=strtolower($arr[3]))//If the town and county name are not same...
                                {
                                    $CountyID  = $CountyDetails['CountyID'];
                                }
                                $CountryID = $CountyDetails['CountryID'];
                            }
                            
                         
                        }    
                        
                        $arr['12'] = $CountyID;
                        $arr['13'] = $CountryID;
                        $address[] = $arr;
                        
                        
                        
                        
                      //$this->controller->log($arr);
                    }
                }
            }
                       
            if (count($address) == 0) {
                $this->controller->log(var_export($result,true));
                throw new Exception('EM1011');
            }
           
            $address2 = $this->sortTwoDimensionArrayByKey($address, '5');
                       
          // $this->controller->log(var_export($address2, true));
            
            return $address2;
        
        
        } catch (Exception $e) {
            $this->error =  $e->getMessage();
            $this->LogError(__METHOD__, $params, $this->error);
            return false;
        }
    }
    
    public function Error() {
        return $this->error;
    }
        
    private function LogError($func, $args, $error) {
        
        $s = $func;
        foreach($args as $key => $value) {
            $s .= "\n$key = $value";
        }
        $s .= "\nError: $error";
        $this->controller->log($s);
    }
    
    
     private  function sortTwoDimensionArrayByKey($arr, $arrKey, $sortOrder=SORT_ASC){
        
        $key_arr = array();
         
        foreach ($arr as $key => $row)
        {
            $key_arr[$key] = $row[$arrKey];
        }
        array_multisort($key_arr, $sortOrder, $arr);
        return $arr;
    }
    
}

?>
