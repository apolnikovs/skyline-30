<?php

/* Note:
   To time the execution of this program uncomment the follwing
   timer code and similar code at the end of this file.
   Execution time will be recorded in /less.text
***********************************************************/
//$time_start = microtime(true);

if (isset($_SERVER['HTTP_ACCEPT_ENCODING']) && substr_count($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip')) {
    ob_start("ob_gzhandler");
} else {
    ob_start();
}

$skin = isset($_GET['skin']) ? $_GET['skin'] : 'default';
$size = isset($_GET['size']) ? $_GET['size'] : '950';

// **********************************************************
// Make sure Skin file is first entry in files array...
// **********************************************************


$files = [
    //"Skins/{$skin}/lessblue/reset.css",
    //"Skins/{$skin}/lessblue/config_{$size}.less", 
    
    "Skins/{$skin}/variables.less", 
    
    //"Skins/{$skin}/lessblue/mixins.less",
    //"Skins/{$skin}/lessblue/typography.less",
    //"Skins/{$skin}/lessblue/forms.less",
    //"Skins/{$skin}/lessblue/grid.less",
    //"Skins/{$skin}/lessblue/tables.less",
    //"Skins/{$skin}/lessblue/extras.less",
    
    "Skins/{$skin}/lessblue.less", 
    "Skins/{$skin}/datatables.less",
    "Skins/{$skin}/email.less",
    "Skins/{$skin}/jobupdate.less",
    "Skins/{$skin}/timeline.less",
    "Skins/{$skin}/screen.less", 
    "Skins/{$skin}/sitemap.less", 
    "Skins/{$skin}/diary.less" 
];



// **********************************************************
// use a separate cache file for each skin...
// **********************************************************

$cache = "cache/{$skin}_{$size}.css"; // cache/default_xxx.css

$time = mktime(0, 0, 0, 21, 5, 1980);

foreach ($files as $file) {
    $fileTime = filemtime($file);

    if ($fileTime > $time) {
        $time = $fileTime;
    }
}

if (file_exists($cache)) {
    $cacheTime = filemtime($cache);
    if ($cacheTime < $time) {
        $time = $cacheTime;
        $recache = true;
    } else {
        $recache = false;
    }
} else {
    $recache = true;
}

if (!$recache && isset($_SERVER['If-Modified-Since']) && strtotime($_SERVER['If-Modified-Since']) >= $time) {
    header("HTTP/1.0 304 Not Modified");
} else {
    if ($recache) {
        require 'lessc.inc.php';
        $lc = new lessc();

        $css = '';
        
        foreach ($files as $file) {
            $css .= file_get_contents($file);
        }

        try {
            $css = $lc->parse($css);
        } catch (Exception $ex) {
            exit('lessc fatal error:<br />'.$ex->getMessage());
        }
        file_put_contents($cache, $css);
    } 
    
    header('Content-type: text/css');
    header('Last-Modified: ' . gmdate("D, d M Y H:i:s", $time) . " GMT");  
    readfile($cache);   
}


/* Note:
   To time the execution of this program uncomment the follwing
   timer code and similar code at the start of this file.
   Execution time will be recorded in /less.text
***********************************************************/
//$time_end = microtime(true);
//$time = round($time_end - $time_start, 4);
//file_put_contents('less.txt',"less compile time = $time\n",FILE_APPEND);

?>
