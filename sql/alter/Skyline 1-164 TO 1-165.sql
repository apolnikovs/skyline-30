# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 

call UpgradeSchemaVersion('1.164');



# ---------------------------------------------------------------------- #
# Modify table "non_skyline_job"                                             #
# ---------------------------------------------------------------------- #
ALTER TABLE `non_skyline_job` ADD COLUMN `NetworkID` INT(11) NULL DEFAULT NULL AFTER `NonSkylineJobID`;
ALTER TABLE `non_skyline_job` ADD COLUMN `NetworkRefNo` VARCHAR(22) NULL DEFAULT NULL AFTER `ServiceProviderJobNo`;



# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
insert into version (VersionNo) values ('1.165');
