# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 
call UpgradeSchemaVersion('1.299');

# ---------------------------------------------------------------------- #
# Vic Changes 															 #
# ---------------------------------------------------------------------- # 
ALTER TABLE user_reports ADD COLUMN UserType VARCHAR(50) NOT NULL AFTER UserID;

# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
insert into version (VersionNo) values ('1.300');



