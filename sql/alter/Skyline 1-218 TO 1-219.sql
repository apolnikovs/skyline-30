# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 
call UpgradeSchemaVersion('1.218');


# ---------------------------------------------------------------------- #
# Modify Table courier                  	                             #
# ---------------------------------------------------------------------- #
alter table courier add column CommunicateIndividualConsignments enum('No','Yes') not null default 'No' after ReportFailureDays;


# ---------------------------------------------------------------------- #
# Modify Table job                  	                                 #
# ---------------------------------------------------------------------- #
ALTER TABLE job MODIFY COLUMN RepairType ENUM('customer','installation','stock');


# ---------------------------------------------------------------------- #
# Modify Table tat_defaults            	                                 #
# ---------------------------------------------------------------------- #
CREATE TABLE IF NOT EXISTS tat_defaults (
										TatID int(11) NOT NULL AUTO_INCREMENT, 
										TatType varchar(20) NOT NULL, 
										Button1 int(11) DEFAULT NULL, 
										Button2 int(11) DEFAULT NULL,
										Button3 int(11) DEFAULT NULL, 
										Button4 int(11) DEFAULT NULL, 
										Button1Colour varchar(10) DEFAULT NULL, 
										Button2Colour varchar(10) DEFAULT NULL, 
										Button3Colour varchar(10) DEFAULT NULL, 
										Button4Colour varchar(10) DEFAULT NULL, 
										UserID int(11) NOT NULL, 
										PRIMARY KEY (TatID), 
										UNIQUE KEY TatType_2 (TatType,UserID) 
										) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
insert into version (VersionNo) values ('1.219');
