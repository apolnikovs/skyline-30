# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 

call UpgradeSchemaVersion('1.158');


# ---------------------------------------------------------------------- #
# Modify table "service_provider"                                                     #
# ---------------------------------------------------------------------- #
ALTER TABLE `service_provider` ADD COLUMN `NumberOfWaypoints` INT(11) NULL DEFAULT NULL;


# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #

insert into version (VersionNo) values ('1.159');




