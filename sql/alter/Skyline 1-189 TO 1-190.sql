# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 

call UpgradeSchemaVersion('1.189');

# ---------------------------------------------------------------------- #
# Add table "network"                                                 #
# ---------------------------------------------------------------------- #
ALTER TABLE `network` ADD COLUMN `SendNetworkReport` ENUM('Yes','No') NULL DEFAULT NULL AFTER `SendASCReport`;


# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #

insert into version (VersionNo) values ('1.190');
