# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 
call UpgradeSchemaVersion('1.280');

# ---------------------------------------------------------------------- #
# Andris Stock Changes                                                   #
# ---------------------------------------------------------------------- # 
ALTER TABLE service_provider ADD COLUMN DocumentCustomColorScheme ENUM('Skyline','Customised') NOT NULL DEFAULT 'Skyline' AFTER CurrencyID, ADD COLUMN DocColour1 VARCHAR(10) NOT NULL DEFAULT '25AAE1' AFTER DocumentCustomColorScheme, ADD COLUMN DocColour2 VARCHAR(10) NOT NULL DEFAULT '0F75BC' AFTER DocColour1, ADD COLUMN DocColour3 VARCHAR(10) NOT NULL DEFAULT '0F75BC' AFTER DocColour2, ADD COLUMN DocLogo VARCHAR(256) NULL DEFAULT NULL AFTER DocColour3;


# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
insert into version (VersionNo) values ('1.281');
