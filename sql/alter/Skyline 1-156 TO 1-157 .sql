# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 

call UpgradeSchemaVersion('1.156');


# ---------------------------------------------------------------------- #
# Modify table "job"                                                     #
# ---------------------------------------------------------------------- #
ALTER TABLE `job` ADD COLUMN `BrandID` INT(11) NULL DEFAULT NULL AFTER `ClientID`;
ALTER TABLE `job` ADD INDEX `BrandID` (`BrandID`);



# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #

insert into version (VersionNo) values ('1.157');




